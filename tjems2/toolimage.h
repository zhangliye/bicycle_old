#ifndef TOOLIMAGE_H
#define TOOLIMAGE_H

#include "pub.h"
#include <QObject>

class VideoView;
class ToolImage : public QObject
{
	Q_OBJECT

public:
	//# parent, send signal
	//#
	VideoView *mView;
	QObject *mSender;               // signal receiver

public:
	ToolImage(VideoView *parent);
	~ToolImage();
	void install( QObject *sender);
	void uninstall( QObject *sender );

public slots:
	void mousePress( QMouseEvent *mouseEvent);
	void mouseMove( QMouseEvent *mouseEvent);
	void mouseRelease( QMouseEvent *mouseEvent);
	void mouseDoubleClick( QMouseEvent *mouseEvent);
	void keyPress( QKeyEvent *keyEvent);
};

#endif // TOOLIMAGE_H
