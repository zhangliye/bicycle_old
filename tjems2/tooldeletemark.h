#ifndef TOOLDELETEMARK_H
#define TOOLDELETEMARK_H

#include "toolimage.h"
#include "videoview.h"

class ToolDeleteMark : public ToolImage
{
	Q_OBJECT

public:
	ToolDeleteMark(VideoView *parent);
	~ToolDeleteMark();

public:
	QVector<QPointF> mBoundaryPts;       //#[(x1, y1), ...]  points of detector boundary

	// temporal drawing
	// 
	QPoint mPrePoint;

	// bicycle loop detector area  	
	QList<QGraphicsEllipseItem*> mPointItems;
	QList<QGraphicsLineItem*> mLineItems;

	public slots:
		void mouseMove( QMouseEvent *mouseEvent);
		void mousePress( QMouseEvent *mouseEvent);
		void keyPress( QKeyEvent *keyEvent);			
};


#endif // TOOLSEMITRACK