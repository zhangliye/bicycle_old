#ifndef TOOLDELETELOOPDETECTOR_H
#define TOOLDELETELOOPDETECTOR_H

#include "toolimage.h"
#include "videoview.h"

class ToolDeleteLoopDetector : public ToolImage
{
	Q_OBJECT

public:
	ToolDeleteLoopDetector(VideoView *parent);
	~ToolDeleteLoopDetector();

public slots:
	void mousePress( QMouseEvent *mouseEvent);
	
};

#endif // TOOLDELETELOOPDETECTOR_H
