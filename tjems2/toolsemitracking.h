#ifndef TOOLSEMITRACKING_H
#define TOOLSEMITRACKING_H

#include "toolimage.h"
#include "videoview.h"

class ToolSemiTracking : public ToolImage
{
	Q_OBJECT

public:
	ToolSemiTracking(VideoView *parent);
	~ToolSemiTracking();

public:
	QVector<QPointF> mBoundaryPts;       //#[(x1, y1), ...]  points of detector boundary

	// temporal drawing
	// 
	QPoint mPrePoint;

	// bicycle loop detector area  	
	QList<QGraphicsEllipseItem*> mPointItems;
	QList<QGraphicsLineItem*> mLineItems;

	public slots:
		void mouseMove( QMouseEvent *mouseEvent);
		void mousePress( QMouseEvent *mouseEvent);
		void keyPress( QKeyEvent *keyEvent);			
};


#endif // TOOLSEMITRACKING_H
