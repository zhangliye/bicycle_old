#include "tooldeletemark.h"
#include "videoview.h"
#include "mainwindow.h"
#include <iostream>
using namespace std;

ToolDeleteMark::ToolDeleteMark(VideoView *parent)
	: ToolImage(parent)
{
	;
}

ToolDeleteMark::~ToolDeleteMark()
{
	;
}

void ToolDeleteMark::mouseMove( QMouseEvent *mouseEvent)
{
	// Extract sum image
	//
	QPointF pt = mView->mapToScene( QPoint(mouseEvent->x(), mouseEvent->y()) );


	// show in current image
	//
}

void ToolDeleteMark::mousePress( QMouseEvent *mouseEvent)
{
	if (Qt::LeftButton == mouseEvent->button())    // left mouse button, add point and line 
	{   // add one point   
		// save point to list
		QPointF pt = mView->mapToScene( QPoint(mouseEvent->x(), mouseEvent->y()) );
		mBoundaryPts.append( pt );
		int small_wind_width = 10;
		float x = pt.x();
		float y = pt.y();
		int x1 = (x - small_wind_width)>=0 ? x-small_wind_width : -(x-small_wind_width);
		int y1 = (y - small_wind_width)>=0 ? y-small_wind_width : -(y-small_wind_width);

		// draw temporal point
		//
		QGraphicsEllipseItem *pointItem = new QGraphicsEllipseItem( pt.x(), pt.y(), 1, 1);
		pointItem->setPen( QPen(Qt::red) );
		mPointItems.append( pointItem );

		mView->scene()->addItem( pointItem );  // show line		
	}	
}

void ToolDeleteMark::keyPress( QKeyEvent *keyEvent)
{
	cout<<"I am here";
}

