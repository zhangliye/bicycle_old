/********************************************************************************
** Form generated from reading UI file 'globalsettingdlg.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GLOBALSETTINGDLG_H
#define UI_GLOBALSETTINGDLG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GlobalSettingDlgBase
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *mEditResolution;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *mBtMeasure;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label_3;
    QSpinBox *mFrameStep;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *mBtSave;
    QPushButton *mBtCancel;

    void setupUi(QDialog *GlobalSettingDlgBase)
    {
        if (GlobalSettingDlgBase->objectName().isEmpty())
            GlobalSettingDlgBase->setObjectName(QString::fromUtf8("GlobalSettingDlgBase"));
        GlobalSettingDlgBase->resize(620, 218);
        verticalLayoutWidget = new QWidget(GlobalSettingDlgBase);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(20, 40, 591, 161));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(verticalLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        mEditResolution = new QLineEdit(verticalLayoutWidget);
        mEditResolution->setObjectName(QString::fromUtf8("mEditResolution"));

        horizontalLayout->addWidget(mEditResolution);

        label_2 = new QLabel(verticalLayoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout->addWidget(label_2);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);

        mBtMeasure = new QPushButton(verticalLayoutWidget);
        mBtMeasure->setObjectName(QString::fromUtf8("mBtMeasure"));

        horizontalLayout_3->addWidget(mBtMeasure);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);

        label_3 = new QLabel(verticalLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_4->addWidget(label_3);

        mFrameStep = new QSpinBox(verticalLayoutWidget);
        mFrameStep->setObjectName(QString::fromUtf8("mFrameStep"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(mFrameStep->sizePolicy().hasHeightForWidth());
        mFrameStep->setSizePolicy(sizePolicy);
        mFrameStep->setMinimumSize(QSize(150, 0));

        horizontalLayout_4->addWidget(mFrameStep);


        verticalLayout->addLayout(horizontalLayout_4);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        mBtSave = new QPushButton(verticalLayoutWidget);
        mBtSave->setObjectName(QString::fromUtf8("mBtSave"));

        horizontalLayout_2->addWidget(mBtSave);

        mBtCancel = new QPushButton(verticalLayoutWidget);
        mBtCancel->setObjectName(QString::fromUtf8("mBtCancel"));

        horizontalLayout_2->addWidget(mBtCancel);


        verticalLayout->addLayout(horizontalLayout_2);


        retranslateUi(GlobalSettingDlgBase);

        QMetaObject::connectSlotsByName(GlobalSettingDlgBase);
    } // setupUi

    void retranslateUi(QDialog *GlobalSettingDlgBase)
    {
        GlobalSettingDlgBase->setWindowTitle(QApplication::translate("GlobalSettingDlgBase", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("GlobalSettingDlgBase", "image resolution", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("GlobalSettingDlgBase", "meter/pixel", 0, QApplication::UnicodeUTF8));
        mBtMeasure->setText(QApplication::translate("GlobalSettingDlgBase", "measurment from iamge", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("GlobalSettingDlgBase", "frame step:", 0, QApplication::UnicodeUTF8));
        mBtSave->setText(QApplication::translate("GlobalSettingDlgBase", "save", 0, QApplication::UnicodeUTF8));
        mBtCancel->setText(QApplication::translate("GlobalSettingDlgBase", "cancel", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class GlobalSettingDlgBase: public Ui_GlobalSettingDlgBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GLOBALSETTINGDLG_H
