/********************************************************************************
** Form generated from reading UI file 'resolutionmeasuredlg.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RESOLUTIONMEASUREDLG_H
#define UI_RESOLUTIONMEASUREDLG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ResolutionMesureDlgBase
{
public:
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *mMainLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *mEditPath;
    QPushButton *mBtVideoPath;
    QHBoxLayout *horizontalLayout_6;
    QCheckBox *mCheckBoxMeasure;
    QSpacerItem *horizontalSpacer_6;
    QHBoxLayout *horizontalLayout_5;
    QLabel *mLabelPosition;
    QSpacerItem *horizontalSpacer_5;
    QLabel *label_4;
    QLabel *mLabelFrameNum;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label_3;
    QLabel *mLabelResolution;
    QLabel *label_6;
    QSpacerItem *horizontalSpacer_4;
    QSlider *mSliderVideo;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *mBtBack;
    QPushButton *mBtNext;
    QSpacerItem *horizontalSpacer;
    QLabel *label_2;
    QSpinBox *mSpinBoxStep;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *mBtOk;
    QPushButton *mBtCancel;

    void setupUi(QDialog *ResolutionMesureDlgBase)
    {
        if (ResolutionMesureDlgBase->objectName().isEmpty())
            ResolutionMesureDlgBase->setObjectName(QString::fromUtf8("ResolutionMesureDlgBase"));
        ResolutionMesureDlgBase->resize(880, 686);
        ResolutionMesureDlgBase->setAutoFillBackground(true);
        verticalLayoutWidget_2 = new QWidget(ResolutionMesureDlgBase);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(10, 10, 851, 671));
        mMainLayout = new QVBoxLayout(verticalLayoutWidget_2);
        mMainLayout->setObjectName(QString::fromUtf8("mMainLayout"));
        mMainLayout->setSizeConstraint(QLayout::SetMaximumSize);
        mMainLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(verticalLayoutWidget_2);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        mEditPath = new QLineEdit(verticalLayoutWidget_2);
        mEditPath->setObjectName(QString::fromUtf8("mEditPath"));

        horizontalLayout->addWidget(mEditPath);

        mBtVideoPath = new QPushButton(verticalLayoutWidget_2);
        mBtVideoPath->setObjectName(QString::fromUtf8("mBtVideoPath"));

        horizontalLayout->addWidget(mBtVideoPath);


        mMainLayout->addLayout(horizontalLayout);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        mCheckBoxMeasure = new QCheckBox(verticalLayoutWidget_2);
        mCheckBoxMeasure->setObjectName(QString::fromUtf8("mCheckBoxMeasure"));

        horizontalLayout_6->addWidget(mCheckBoxMeasure);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_6);


        mMainLayout->addLayout(horizontalLayout_6);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        mLabelPosition = new QLabel(verticalLayoutWidget_2);
        mLabelPosition->setObjectName(QString::fromUtf8("mLabelPosition"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(mLabelPosition->sizePolicy().hasHeightForWidth());
        mLabelPosition->setSizePolicy(sizePolicy);
        mLabelPosition->setMinimumSize(QSize(80, 0));

        horizontalLayout_5->addWidget(mLabelPosition);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_5);

        label_4 = new QLabel(verticalLayoutWidget_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        sizePolicy.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy);

        horizontalLayout_5->addWidget(label_4);

        mLabelFrameNum = new QLabel(verticalLayoutWidget_2);
        mLabelFrameNum->setObjectName(QString::fromUtf8("mLabelFrameNum"));
        sizePolicy.setHeightForWidth(mLabelFrameNum->sizePolicy().hasHeightForWidth());
        mLabelFrameNum->setSizePolicy(sizePolicy);
        mLabelFrameNum->setMinimumSize(QSize(80, 0));

        horizontalLayout_5->addWidget(mLabelFrameNum);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_3);

        label_3 = new QLabel(verticalLayoutWidget_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        sizePolicy.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy);

        horizontalLayout_5->addWidget(label_3);

        mLabelResolution = new QLabel(verticalLayoutWidget_2);
        mLabelResolution->setObjectName(QString::fromUtf8("mLabelResolution"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(80);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(mLabelResolution->sizePolicy().hasHeightForWidth());
        mLabelResolution->setSizePolicy(sizePolicy1);
        mLabelResolution->setMinimumSize(QSize(80, 0));

        horizontalLayout_5->addWidget(mLabelResolution);

        label_6 = new QLabel(verticalLayoutWidget_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        sizePolicy.setHeightForWidth(label_6->sizePolicy().hasHeightForWidth());
        label_6->setSizePolicy(sizePolicy);

        horizontalLayout_5->addWidget(label_6);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_4);


        mMainLayout->addLayout(horizontalLayout_5);

        mSliderVideo = new QSlider(verticalLayoutWidget_2);
        mSliderVideo->setObjectName(QString::fromUtf8("mSliderVideo"));
        mSliderVideo->setOrientation(Qt::Horizontal);

        mMainLayout->addWidget(mSliderVideo);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        mBtBack = new QPushButton(verticalLayoutWidget_2);
        mBtBack->setObjectName(QString::fromUtf8("mBtBack"));

        horizontalLayout_2->addWidget(mBtBack);

        mBtNext = new QPushButton(verticalLayoutWidget_2);
        mBtNext->setObjectName(QString::fromUtf8("mBtNext"));

        horizontalLayout_2->addWidget(mBtNext);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        label_2 = new QLabel(verticalLayoutWidget_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        mSpinBoxStep = new QSpinBox(verticalLayoutWidget_2);
        mSpinBoxStep->setObjectName(QString::fromUtf8("mSpinBoxStep"));
        mSpinBoxStep->setMinimum(1);

        horizontalLayout_2->addWidget(mSpinBoxStep);


        mMainLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);

        mBtOk = new QPushButton(verticalLayoutWidget_2);
        mBtOk->setObjectName(QString::fromUtf8("mBtOk"));

        horizontalLayout_3->addWidget(mBtOk);

        mBtCancel = new QPushButton(verticalLayoutWidget_2);
        mBtCancel->setObjectName(QString::fromUtf8("mBtCancel"));

        horizontalLayout_3->addWidget(mBtCancel);


        mMainLayout->addLayout(horizontalLayout_3);


        retranslateUi(ResolutionMesureDlgBase);

        QMetaObject::connectSlotsByName(ResolutionMesureDlgBase);
    } // setupUi

    void retranslateUi(QDialog *ResolutionMesureDlgBase)
    {
        ResolutionMesureDlgBase->setWindowTitle(QApplication::translate("ResolutionMesureDlgBase", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("ResolutionMesureDlgBase", "video path:", 0, QApplication::UnicodeUTF8));
        mBtVideoPath->setText(QApplication::translate("ResolutionMesureDlgBase", "...video path", 0, QApplication::UnicodeUTF8));
        mCheckBoxMeasure->setText(QApplication::translate("ResolutionMesureDlgBase", "measure", 0, QApplication::UnicodeUTF8));
        mLabelPosition->setText(QString());
        label_4->setText(QApplication::translate("ResolutionMesureDlgBase", "Frame Number", 0, QApplication::UnicodeUTF8));
        mLabelFrameNum->setText(QApplication::translate("ResolutionMesureDlgBase", "0", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("ResolutionMesureDlgBase", "Resolusion:", 0, QApplication::UnicodeUTF8));
        mLabelResolution->setText(QApplication::translate("ResolutionMesureDlgBase", "0", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("ResolutionMesureDlgBase", "meters/pixel", 0, QApplication::UnicodeUTF8));
        mBtBack->setText(QApplication::translate("ResolutionMesureDlgBase", "<<Back", 0, QApplication::UnicodeUTF8));
        mBtNext->setText(QApplication::translate("ResolutionMesureDlgBase", "Next >>", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("ResolutionMesureDlgBase", "step", 0, QApplication::UnicodeUTF8));
        mBtOk->setText(QApplication::translate("ResolutionMesureDlgBase", "Ok", 0, QApplication::UnicodeUTF8));
        mBtCancel->setText(QApplication::translate("ResolutionMesureDlgBase", "Cancel", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ResolutionMesureDlgBase: public Ui_ResolutionMesureDlgBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RESOLUTIONMEASUREDLG_H
