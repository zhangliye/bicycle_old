/********************************************************************************
** Form generated from reading UI file 'coordinateedtwnd.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_COORDINATEEDTWND_H
#define UI_COORDINATEEDTWND_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_CoordinateEdtWnd
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QLabel *label_2;
    QLineEdit *mEdtY;
    QLabel *label;
    QLineEdit *mEdtX;
    QDialogButtonBox *mBtBox;

    void setupUi(QDialog *CoordinateEdtWnd)
    {
        if (CoordinateEdtWnd->objectName().isEmpty())
            CoordinateEdtWnd->setObjectName(QString::fromUtf8("CoordinateEdtWnd"));
        CoordinateEdtWnd->resize(299, 110);
        verticalLayout = new QVBoxLayout(CoordinateEdtWnd);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_2 = new QLabel(CoordinateEdtWnd);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        mEdtY = new QLineEdit(CoordinateEdtWnd);
        mEdtY->setObjectName(QString::fromUtf8("mEdtY"));

        gridLayout->addWidget(mEdtY, 1, 1, 1, 1);

        label = new QLabel(CoordinateEdtWnd);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        mEdtX = new QLineEdit(CoordinateEdtWnd);
        mEdtX->setObjectName(QString::fromUtf8("mEdtX"));

        gridLayout->addWidget(mEdtX, 0, 1, 1, 1);


        verticalLayout->addLayout(gridLayout);

        mBtBox = new QDialogButtonBox(CoordinateEdtWnd);
        mBtBox->setObjectName(QString::fromUtf8("mBtBox"));
        mBtBox->setOrientation(Qt::Horizontal);
        mBtBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(mBtBox);


        retranslateUi(CoordinateEdtWnd);
        QObject::connect(mBtBox, SIGNAL(accepted()), CoordinateEdtWnd, SLOT(accept()));
        QObject::connect(mBtBox, SIGNAL(rejected()), CoordinateEdtWnd, SLOT(reject()));

        QMetaObject::connectSlotsByName(CoordinateEdtWnd);
    } // setupUi

    void retranslateUi(QDialog *CoordinateEdtWnd)
    {
        CoordinateEdtWnd->setWindowTitle(QApplication::translate("CoordinateEdtWnd", "word coordinate value", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("CoordinateEdtWnd", "y", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("CoordinateEdtWnd", "x", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class CoordinateEdtWnd: public Ui_CoordinateEdtWnd {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_COORDINATEEDTWND_H
