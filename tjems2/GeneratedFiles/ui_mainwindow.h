/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QStatusBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpen_Video_Files;
    QAction *actionOpen_Current_Video;
    QAction *actionExit;
    QAction *actionWebsit;
    QAction *actionManual;
    QAction *actionAbout;
    QAction *actionNext_Frame;
    QAction *actionFinish_processing;
    QAction *actionTest;
    QAction *actionSetting;
    QAction *action50;
    QAction *action100;
    QAction *action150;
    QAction *action200;
    QAction *action250;
    QAction *action300;
    QAction *action350;
    QAction *action400;
    QAction *actionFit_window;
    QAction *action25;
    QAction *mMenuViewPlot;
    QAction *mMenuViewMatrix;
    QAction *actionBottom_window;
    QAction *mMenuViewBottom;
    QAction *mActionLoopDetector;
    QAction *mActionDetectionTrack;
    QAction *mActionExploringModel;
    QWidget *centralwidget;
    QMenuBar *menubar;
    QMenu *menuFile;
    QMenu *menuHelp;
    QMenu *menuSettings;
    QMenu *menuView;
    QMenu *menuVideo_Processing;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(800, 600);
        actionOpen_Video_Files = new QAction(MainWindow);
        actionOpen_Video_Files->setObjectName(QString::fromUtf8("actionOpen_Video_Files"));
        actionOpen_Current_Video = new QAction(MainWindow);
        actionOpen_Current_Video->setObjectName(QString::fromUtf8("actionOpen_Current_Video"));
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        actionWebsit = new QAction(MainWindow);
        actionWebsit->setObjectName(QString::fromUtf8("actionWebsit"));
        actionManual = new QAction(MainWindow);
        actionManual->setObjectName(QString::fromUtf8("actionManual"));
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        actionNext_Frame = new QAction(MainWindow);
        actionNext_Frame->setObjectName(QString::fromUtf8("actionNext_Frame"));
        actionFinish_processing = new QAction(MainWindow);
        actionFinish_processing->setObjectName(QString::fromUtf8("actionFinish_processing"));
        actionTest = new QAction(MainWindow);
        actionTest->setObjectName(QString::fromUtf8("actionTest"));
        actionSetting = new QAction(MainWindow);
        actionSetting->setObjectName(QString::fromUtf8("actionSetting"));
        action50 = new QAction(MainWindow);
        action50->setObjectName(QString::fromUtf8("action50"));
        action100 = new QAction(MainWindow);
        action100->setObjectName(QString::fromUtf8("action100"));
        action150 = new QAction(MainWindow);
        action150->setObjectName(QString::fromUtf8("action150"));
        action200 = new QAction(MainWindow);
        action200->setObjectName(QString::fromUtf8("action200"));
        action250 = new QAction(MainWindow);
        action250->setObjectName(QString::fromUtf8("action250"));
        action300 = new QAction(MainWindow);
        action300->setObjectName(QString::fromUtf8("action300"));
        action350 = new QAction(MainWindow);
        action350->setObjectName(QString::fromUtf8("action350"));
        action400 = new QAction(MainWindow);
        action400->setObjectName(QString::fromUtf8("action400"));
        actionFit_window = new QAction(MainWindow);
        actionFit_window->setObjectName(QString::fromUtf8("actionFit_window"));
        action25 = new QAction(MainWindow);
        action25->setObjectName(QString::fromUtf8("action25"));
        mMenuViewPlot = new QAction(MainWindow);
        mMenuViewPlot->setObjectName(QString::fromUtf8("mMenuViewPlot"));
        mMenuViewPlot->setCheckable(true);
        mMenuViewPlot->setChecked(true);
        mMenuViewMatrix = new QAction(MainWindow);
        mMenuViewMatrix->setObjectName(QString::fromUtf8("mMenuViewMatrix"));
        mMenuViewMatrix->setCheckable(true);
        mMenuViewMatrix->setChecked(true);
        actionBottom_window = new QAction(MainWindow);
        actionBottom_window->setObjectName(QString::fromUtf8("actionBottom_window"));
        mMenuViewBottom = new QAction(MainWindow);
        mMenuViewBottom->setObjectName(QString::fromUtf8("mMenuViewBottom"));
        mMenuViewBottom->setCheckable(true);
        mMenuViewBottom->setChecked(true);
        mActionLoopDetector = new QAction(MainWindow);
        mActionLoopDetector->setObjectName(QString::fromUtf8("mActionLoopDetector"));
        mActionLoopDetector->setCheckable(true);
        mActionDetectionTrack = new QAction(MainWindow);
        mActionDetectionTrack->setObjectName(QString::fromUtf8("mActionDetectionTrack"));
        mActionDetectionTrack->setCheckable(true);
        mActionExploringModel = new QAction(MainWindow);
        mActionExploringModel->setObjectName(QString::fromUtf8("mActionExploringModel"));
        mActionExploringModel->setCheckable(true);
        mActionExploringModel->setChecked(true);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 26));
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuHelp = new QMenu(menubar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        menuSettings = new QMenu(menubar);
        menuSettings->setObjectName(QString::fromUtf8("menuSettings"));
        menuView = new QMenu(menubar);
        menuView->setObjectName(QString::fromUtf8("menuView"));
        menuVideo_Processing = new QMenu(menubar);
        menuVideo_Processing->setObjectName(QString::fromUtf8("menuVideo_Processing"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menuFile->menuAction());
        menubar->addAction(menuView->menuAction());
        menubar->addAction(menuVideo_Processing->menuAction());
        menubar->addAction(menuSettings->menuAction());
        menubar->addAction(menuHelp->menuAction());
        menuFile->addAction(actionOpen_Video_Files);
        menuFile->addAction(actionOpen_Current_Video);
        menuFile->addSeparator();
        menuFile->addAction(actionExit);
        menuHelp->addAction(actionWebsit);
        menuHelp->addAction(actionManual);
        menuHelp->addSeparator();
        menuHelp->addAction(actionAbout);
        menuSettings->addAction(actionSetting);
        menuSettings->addSeparator();
        menuSettings->addAction(mActionExploringModel);
        menuSettings->addAction(mActionLoopDetector);
        menuSettings->addAction(mActionDetectionTrack);
        menuVideo_Processing->addAction(actionNext_Frame);
        menuVideo_Processing->addSeparator();
        menuVideo_Processing->addAction(actionFinish_processing);
        menuVideo_Processing->addAction(actionBottom_window);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QString());
        actionOpen_Video_Files->setText(QApplication::translate("MainWindow", "open new video", 0, QApplication::UnicodeUTF8));
        actionOpen_Current_Video->setText(QApplication::translate("MainWindow", "open current video", 0, QApplication::UnicodeUTF8));
        actionExit->setText(QApplication::translate("MainWindow", "Exit", 0, QApplication::UnicodeUTF8));
        actionWebsit->setText(QApplication::translate("MainWindow", "Websit", 0, QApplication::UnicodeUTF8));
        actionManual->setText(QApplication::translate("MainWindow", "Manual", 0, QApplication::UnicodeUTF8));
        actionAbout->setText(QApplication::translate("MainWindow", "About", 0, QApplication::UnicodeUTF8));
        actionNext_Frame->setText(QApplication::translate("MainWindow", "Next Frame (Space)", 0, QApplication::UnicodeUTF8));
        actionFinish_processing->setText(QApplication::translate("MainWindow", "Finish processing", 0, QApplication::UnicodeUTF8));
        actionTest->setText(QApplication::translate("MainWindow", "test", 0, QApplication::UnicodeUTF8));
        actionSetting->setText(QApplication::translate("MainWindow", "setting", 0, QApplication::UnicodeUTF8));
        action50->setText(QApplication::translate("MainWindow", "50", 0, QApplication::UnicodeUTF8));
        action100->setText(QApplication::translate("MainWindow", "100", 0, QApplication::UnicodeUTF8));
        action150->setText(QApplication::translate("MainWindow", "150", 0, QApplication::UnicodeUTF8));
        action200->setText(QApplication::translate("MainWindow", "200", 0, QApplication::UnicodeUTF8));
        action250->setText(QApplication::translate("MainWindow", "250", 0, QApplication::UnicodeUTF8));
        action300->setText(QApplication::translate("MainWindow", "300", 0, QApplication::UnicodeUTF8));
        action350->setText(QApplication::translate("MainWindow", "350", 0, QApplication::UnicodeUTF8));
        action400->setText(QApplication::translate("MainWindow", "400", 0, QApplication::UnicodeUTF8));
        actionFit_window->setText(QApplication::translate("MainWindow", "fit window", 0, QApplication::UnicodeUTF8));
        action25->setText(QApplication::translate("MainWindow", "25", 0, QApplication::UnicodeUTF8));
        mMenuViewPlot->setText(QApplication::translate("MainWindow", "plot window", 0, QApplication::UnicodeUTF8));
        mMenuViewMatrix->setText(QApplication::translate("MainWindow", "matrix window", 0, QApplication::UnicodeUTF8));
        actionBottom_window->setText(QApplication::translate("MainWindow", "bottom window", 0, QApplication::UnicodeUTF8));
        mMenuViewBottom->setText(QApplication::translate("MainWindow", "bottom window", 0, QApplication::UnicodeUTF8));
        mActionLoopDetector->setText(QApplication::translate("MainWindow", "Loop detector model", 0, QApplication::UnicodeUTF8));
        mActionDetectionTrack->setText(QApplication::translate("MainWindow", "Detection and Track model", 0, QApplication::UnicodeUTF8));
        mActionExploringModel->setText(QApplication::translate("MainWindow", "Exploring model", 0, QApplication::UnicodeUTF8));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0, QApplication::UnicodeUTF8));
        menuHelp->setTitle(QApplication::translate("MainWindow", "Help", 0, QApplication::UnicodeUTF8));
        menuSettings->setTitle(QApplication::translate("MainWindow", "Settings", 0, QApplication::UnicodeUTF8));
        menuView->setTitle(QApplication::translate("MainWindow", "View", 0, QApplication::UnicodeUTF8));
        menuVideo_Processing->setTitle(QApplication::translate("MainWindow", "Video Processing", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
