/********************************************************************************
** Form generated from reading UI file 'matwnd.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MATWND_H
#define UI_MATWND_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MatWnd
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *mVLayout;

    void setupUi(QWidget *MatWnd)
    {
        if (MatWnd->objectName().isEmpty())
            MatWnd->setObjectName(QString::fromUtf8("MatWnd"));
        MatWnd->resize(322, 490);
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MatWnd->sizePolicy().hasHeightForWidth());
        MatWnd->setSizePolicy(sizePolicy);
        MatWnd->setAutoFillBackground(true);
        gridLayout = new QGridLayout(MatWnd);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        mVLayout = new QVBoxLayout();
        mVLayout->setObjectName(QString::fromUtf8("mVLayout"));

        gridLayout->addLayout(mVLayout, 0, 0, 1, 1);


        retranslateUi(MatWnd);

        QMetaObject::connectSlotsByName(MatWnd);
    } // setupUi

    void retranslateUi(QWidget *MatWnd)
    {
        MatWnd->setWindowTitle(QString());
    } // retranslateUi

};

namespace Ui {
    class MatWnd: public Ui_MatWnd {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MATWND_H
