/********************************************************************************
** Form generated from reading UI file 'bottomwnd.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BOTTOMWND_H
#define UI_BOTTOMWND_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QListWidget>
#include <QtGui/QTabWidget>
#include <QtGui/QTableWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BottomWnd
{
public:
    QGridLayout *gridLayout;
    QTabWidget *mTabShow;
    QWidget *tabLog;
    QHBoxLayout *horizontalLayout_4;
    QListWidget *mLstWidLog;
    QWidget *tabTraj;
    QHBoxLayout *horizontalLayout;
    QTableWidget *mTableTrajectory;
    QWidget *tabControl;
    QHBoxLayout *horizontalLayout_2;
    QTableWidget *mTabControl;

    void setupUi(QWidget *BottomWnd)
    {
        if (BottomWnd->objectName().isEmpty())
            BottomWnd->setObjectName(QString::fromUtf8("BottomWnd"));
        BottomWnd->resize(895, 100);
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(BottomWnd->sizePolicy().hasHeightForWidth());
        BottomWnd->setSizePolicy(sizePolicy);
        BottomWnd->setMinimumSize(QSize(0, 100));
        BottomWnd->setAutoFillBackground(true);
        gridLayout = new QGridLayout(BottomWnd);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        mTabShow = new QTabWidget(BottomWnd);
        mTabShow->setObjectName(QString::fromUtf8("mTabShow"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(mTabShow->sizePolicy().hasHeightForWidth());
        mTabShow->setSizePolicy(sizePolicy1);
        tabLog = new QWidget();
        tabLog->setObjectName(QString::fromUtf8("tabLog"));
        horizontalLayout_4 = new QHBoxLayout(tabLog);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        mLstWidLog = new QListWidget(tabLog);
        mLstWidLog->setObjectName(QString::fromUtf8("mLstWidLog"));
        sizePolicy1.setHeightForWidth(mLstWidLog->sizePolicy().hasHeightForWidth());
        mLstWidLog->setSizePolicy(sizePolicy1);

        horizontalLayout_4->addWidget(mLstWidLog);

        mTabShow->addTab(tabLog, QString());
        tabTraj = new QWidget();
        tabTraj->setObjectName(QString::fromUtf8("tabTraj"));
        horizontalLayout = new QHBoxLayout(tabTraj);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        mTableTrajectory = new QTableWidget(tabTraj);
        mTableTrajectory->setObjectName(QString::fromUtf8("mTableTrajectory"));

        horizontalLayout->addWidget(mTableTrajectory);

        mTabShow->addTab(tabTraj, QString());
        tabControl = new QWidget();
        tabControl->setObjectName(QString::fromUtf8("tabControl"));
        horizontalLayout_2 = new QHBoxLayout(tabControl);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        mTabControl = new QTableWidget(tabControl);
        mTabControl->setObjectName(QString::fromUtf8("mTabControl"));

        horizontalLayout_2->addWidget(mTabControl);

        mTabShow->addTab(tabControl, QString());

        gridLayout->addWidget(mTabShow, 0, 0, 1, 1);


        retranslateUi(BottomWnd);

        mTabShow->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(BottomWnd);
    } // setupUi

    void retranslateUi(QWidget *BottomWnd)
    {
        BottomWnd->setWindowTitle(QString());
        mTabShow->setTabText(mTabShow->indexOf(tabLog), QApplication::translate("BottomWnd", "Log", 0, QApplication::UnicodeUTF8));
        mTabShow->setTabText(mTabShow->indexOf(tabTraj), QApplication::translate("BottomWnd", "trjactory", 0, QApplication::UnicodeUTF8));
        mTabShow->setTabText(mTabShow->indexOf(tabControl), QApplication::translate("BottomWnd", "control pont", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class BottomWnd: public Ui_BottomWnd {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BOTTOMWND_H
