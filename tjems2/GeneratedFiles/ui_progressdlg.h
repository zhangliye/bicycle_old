/********************************************************************************
** Form generated from reading UI file 'progressdlg.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROGRESSDLG_H
#define UI_PROGRESSDLG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QProgressBar>

QT_BEGIN_NAMESPACE

class Ui_ProgressDlg
{
public:
    QHBoxLayout *horizontalLayout_2;
    QHBoxLayout *horizontalLayout;
    QProgressBar *mProgressBar;

    void setupUi(QDialog *ProgressDlg)
    {
        if (ProgressDlg->objectName().isEmpty())
            ProgressDlg->setObjectName(QString::fromUtf8("ProgressDlg"));
        ProgressDlg->resize(853, 48);
        horizontalLayout_2 = new QHBoxLayout(ProgressDlg);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        mProgressBar = new QProgressBar(ProgressDlg);
        mProgressBar->setObjectName(QString::fromUtf8("mProgressBar"));
        mProgressBar->setValue(24);

        horizontalLayout->addWidget(mProgressBar);


        horizontalLayout_2->addLayout(horizontalLayout);


        retranslateUi(ProgressDlg);

        QMetaObject::connectSlotsByName(ProgressDlg);
    } // setupUi

    void retranslateUi(QDialog *ProgressDlg)
    {
        ProgressDlg->setWindowTitle(QApplication::translate("ProgressDlg", "progress", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ProgressDlg: public Ui_ProgressDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROGRESSDLG_H
