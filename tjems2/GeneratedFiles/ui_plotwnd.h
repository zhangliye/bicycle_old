/********************************************************************************
** Form generated from reading UI file 'plotwnd.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PLOTWND_H
#define UI_PLOTWND_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDockWidget>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_PlotWnd
{
public:
    QWidget *dockWidgetContents;
    QHBoxLayout *horizontalLayout;
    QCustomPlot *mCustomPlot;

    void setupUi(QDockWidget *PlotWnd)
    {
        if (PlotWnd->objectName().isEmpty())
            PlotWnd->setObjectName(QString::fromUtf8("PlotWnd"));
        PlotWnd->resize(477, 136);
        PlotWnd->setMinimumSize(QSize(91, 100));
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QString::fromUtf8("dockWidgetContents"));
        horizontalLayout = new QHBoxLayout(dockWidgetContents);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        mCustomPlot = new QCustomPlot(dockWidgetContents);
        mCustomPlot->setObjectName(QString::fromUtf8("mCustomPlot"));

        horizontalLayout->addWidget(mCustomPlot);

        PlotWnd->setWidget(dockWidgetContents);

        retranslateUi(PlotWnd);

        QMetaObject::connectSlotsByName(PlotWnd);
    } // setupUi

    void retranslateUi(QDockWidget *PlotWnd)
    {
        PlotWnd->setWindowTitle(QString());
    } // retranslateUi

};

namespace Ui {
    class PlotWnd: public Ui_PlotWnd {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PLOTWND_H
